package be.janolaerts.corespringopdracht15.housekeeping.services.garden;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringBootTest(classes=GardeningServiceTestConfig.class)
public class GardeningServiceTest {

    private GardeningServiceImpl testGardening;

    @Autowired
    private GardeningToolMock mock;

    @Autowired
    @Qualifier("testGardening")
    public void setTestGardening(GardeningService testGardening) {
        this.testGardening = (GardeningServiceImpl) testGardening;
    }

    @Test
    @DirtiesContext
    public void testDoJob() {
        testGardening.garden();
        Assertions.assertTrue(mock.isCalled());
    }
}