package be.janolaerts.corespringopdracht15.housekeeping.services.domestic;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
@ComponentScan(value = "be.janolaerts.corespringopdracht15",
        excludeFilters=@Filter(type=FilterType.REGEX, pattern=".*TestConfig.*"))
@EnableAspectJAutoProxy
public class DomesticServiceTestConfig {

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource ms =
                new ResourceBundleMessageSource();
        ms.setBasename("housekeeping");
        return ms;
    }
}
