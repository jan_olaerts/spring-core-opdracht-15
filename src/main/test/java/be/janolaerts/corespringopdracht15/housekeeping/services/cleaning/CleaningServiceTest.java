package be.janolaerts.corespringopdracht15.housekeeping.services.cleaning;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringBootTest(classes=CleaningServiceTestConfig.class)
@TestPropertySource(properties={"rate=10"})
public class CleaningServiceTest {

    private CleaningServiceImpl testCleaner;

    @Autowired
    private CleaningToolMock mock;

    @Autowired
    @Qualifier("testCleaner")
    public void setTestCleaner(CleaningService testCleaner) {
        this.testCleaner = (CleaningServiceImpl) testCleaner;
    }

    @Test
    @DirtiesContext
    public void testDoJob() {
        testCleaner.clean();
        Assertions.assertTrue(mock.isCalled());
    }
}