package be.janolaerts.corespringopdracht15.housekeeping.services.garden;

public class GardeningToolMock implements GardeningTool {

    private boolean gardeningCalled = false;

    public void doGardenJob() {
        gardeningCalled = true;
    }

    public boolean isCalled() {
        return gardeningCalled;
    }
}