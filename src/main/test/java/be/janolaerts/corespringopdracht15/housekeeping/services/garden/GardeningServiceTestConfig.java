package be.janolaerts.corespringopdracht15.housekeeping.services.garden;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class GardeningServiceTestConfig {

    @Bean
    public GardeningToolMock mock() {
        return new GardeningToolMock();
    }

    @Bean
    public GardeningService testGardening() {
        return new GardeningServiceImpl();
    }
}