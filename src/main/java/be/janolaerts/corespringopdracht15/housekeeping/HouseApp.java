package be.janolaerts.corespringopdracht15.housekeeping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages="be.janolaerts.corespringopdracht15")
public class HouseApp {

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
                SpringApplication.run(HouseApp.class);

        ctx.start();
//        SpringApplication.exit(ctx, () -> 0);
    }
}