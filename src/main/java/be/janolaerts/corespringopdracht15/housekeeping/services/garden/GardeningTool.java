package be.janolaerts.corespringopdracht15.housekeeping.services.garden;

public interface GardeningTool {

    void doGardenJob();
}