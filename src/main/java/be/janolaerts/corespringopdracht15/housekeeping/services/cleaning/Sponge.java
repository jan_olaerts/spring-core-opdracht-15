package be.janolaerts.corespringopdracht15.housekeeping.services.cleaning;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope("prototype")
@Profile("bigHouse")
public class Sponge implements CleaningTool {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());

    @Override
    public void doCleanJob() {
        logger.info("Cleaning with sponge");
    }
}