package be.janolaerts.corespringopdracht15.housekeeping.services.garden;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import be.janolaerts.corespringopdracht15.lunch.LunchEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Component
@Scope
@Qualifier("gardeningService")
public class GardeningServiceImpl implements GardeningService {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());
    private GardeningTool tool;

    @Autowired
    public void setGardeningTool(GardeningTool tool) {
        this.tool = tool;
    }

    @Override
    public void garden() {
        logger.info("Working in the garden.");
        tool.doGardenJob();
    }

    @EventListener(classes=LunchEvent.class)
    public void onLunchEvent(LunchEvent e) {
        logger.info("Taking a break for lunch");
    }

    @PostConstruct
    public void start() {
        logger.info("Starting GardeningService.");
    }

    @PreDestroy
    public void stop() {
        logger.info("Stopping GardeningService.");
    }
}