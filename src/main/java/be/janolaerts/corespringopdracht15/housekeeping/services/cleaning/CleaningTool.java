package be.janolaerts.corespringopdracht15.housekeeping.services.cleaning;

public interface CleaningTool {

    void doCleanJob();
}