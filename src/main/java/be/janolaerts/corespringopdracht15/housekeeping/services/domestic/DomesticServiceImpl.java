package be.janolaerts.corespringopdracht15.housekeeping.services.domestic;

import be.janolaerts.corespringopdracht15.housekeeping.services.cleaning.CleaningService;
import be.janolaerts.corespringopdracht15.housekeeping.services.garden.GardeningService;
import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import be.janolaerts.corespringopdracht15.lunch.LunchEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Locale;
import java.util.logging.Logger;

@Component("domesticService")
public class DomesticServiceImpl implements DomesticService {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());
    private CleaningService cs;
    private GardeningService gs;
    private MessageSource ms;
    private Locale locale;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    public void setCleaningService(CleaningService cs) {
        this.cs = cs;
    }

    @Autowired
    @Qualifier("gardeningService")
    public void setGardeningService(GardeningService gs) {
        this.gs = gs;
    }

    @Autowired
    public void setMessageSource(MessageSource ms) {
        this.ms = ms;
    }

    @Value("#{T(java.util.Locale).setDefault('nl')}")
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    @EventListener(classes=ContextStartedEvent.class)
    @Order(1)
    public void runHousehold() {
        logger.info("Running household");
        cs.clean();
        gs.garden();
        publisher.publishEvent(new LunchEvent());
    }

    @PostConstruct
    public void start() {
        logger.info("Starting DomesticService.");
        logger.info(ms.getMessage("welcome", new Object[] {12}, locale));
    }

    @PreDestroy
    public void stop() {
        logger.info("Stopping DomesticService.");
    }
}