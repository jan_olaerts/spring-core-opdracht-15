package be.janolaerts.corespringopdracht15.housekeeping.services.domestic;

public interface DomesticService {

    void runHousehold();
}