package be.janolaerts.corespringopdracht15.housekeeping.services.cleaning;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope(value = "prototype", proxyMode= ScopedProxyMode.INTERFACES)
@Order(4)
@Profile("bigHouse")
public class DisposableDuster implements CleaningTool {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());
    private boolean used;

    @Override
    public void doCleanJob() {

        if(used) {
            logger.info("I'm already used. Please throw me away!");

        } else {
            logger.info("Wipe the dust away");
            used = true;
        }
    }
}