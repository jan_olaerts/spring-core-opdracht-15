package be.janolaerts.corespringopdracht15.housekeeping.services.cleaning;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.logging.Logger;

@Component
@Profile("bigHouse")
public class CleaningRobot implements CleaningService {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());
    private List<CleaningTool> tools;

    @Autowired
    public void setCleaningTools(List<CleaningTool> tools) {
        this.tools = tools;
    }

    @Override
    public void clean() {
        logger.info("CleaningRobot cleaning the house");
        tools.forEach(CleaningTool::doCleanJob);
    }

    @PostConstruct
    public void start() {
        logger.info("Starting CleaningRobot.");
    }

    @PreDestroy
    public void stop() {
        logger.info("Stopping CleaningRobot.");
    }
}
