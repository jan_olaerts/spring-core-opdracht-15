package be.janolaerts.corespringopdracht15.housekeeping.services.garden;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.logging.Logger;

@Component
public class HedgeTrimmerFactory {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());
    private int hour = LocalTime.now().getHour();
    private int minHour;
    private int maxHour;

    @Value("${minHour}")
    public void setMinHour(int minHour) {
        this.minHour = minHour;
    }

    @Value("${maxHour}")
    public void setMaxHour(int maxHour) {
        this.maxHour = maxHour;
    }

    @Bean
    @Primary
    public GardeningTool hedgeTrimmer() {

        if(hour > minHour && hour < maxHour)
            return () -> logger.info("Trimming hedge electrically");

        else
            return () -> logger.info("Trimming hedge manually");
    }
}