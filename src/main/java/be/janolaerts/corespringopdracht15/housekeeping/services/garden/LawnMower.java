package be.janolaerts.corespringopdracht15.housekeeping.services.garden;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Profile("smallHouse")
public class LawnMower implements GardeningTool {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());

    @Override
    public void doGardenJob() {
        logger.info("Mowing the lawn");
    }
}