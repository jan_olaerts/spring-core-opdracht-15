package be.janolaerts.corespringopdracht15.housekeeping.services.cleaning;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope("prototype")
@Order(2)
@Profile("bigHouse")
public class VacuumCleaner implements CleaningTool {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());

    @Override
    public void doCleanJob() {
        logger.info("Zuuuuuuuuuuuu");
    }
}