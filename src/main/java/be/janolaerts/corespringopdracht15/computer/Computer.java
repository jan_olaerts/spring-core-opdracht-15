package be.janolaerts.corespringopdracht15.computer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;

@Component
public class Computer {

    private LocalDate purchaseDate;
    private URL vendorUrl;
    private double price;
    private int memory;
    private String cpu;
    private int processors;
    private String ownerName;
    private String os;
    private List<String> features;

    public Computer() {
    }

    public Computer(LocalDate purchaseDate, URL vendorUrl, double price, int memory, String cpu, int processors, String ownerName, String os, List<String> features) {
        this.purchaseDate = purchaseDate;
        this.vendorUrl = vendorUrl;
        this.price = price;
        this.memory = memory;
        this.cpu = cpu;
        this.processors = processors;
        this.ownerName = ownerName;
        this.os = os;
        this.features = features;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    @Value("#{T(java.time.LocalDate).parse('${purchaseDate}')}")
    public void setPurchaseDate(LocalDate purchaseDate) {
        if (purchaseDate != null && !purchaseDate.isAfter(LocalDate.now()))
            this.purchaseDate = purchaseDate;
    }

    public URL getVendorUrl() {
        return vendorUrl;
    }

    @Value("#{'http://www.bol.com'}")
    public void setVendorUrl(URL vendorUrl) {
        this.vendorUrl = vendorUrl;
    }

    public double getPrice() {
        return price;
    }

    @Value("#{1500.25}")
    public void setPrice(double price) {
        this.price = price;
    }

    public int getMemory() {
        return memory;
    }

    @Value("#{24}")
    public void setMemory(int memory) {
        this.memory = memory;
    }

    public String getCpu() {
        return cpu;
    }

    @Value("#{systemProperties['sun.cpu.isalist']}")
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public int getProcessors() {
        return processors;
    }

    @Value("#{systemEnvironment['NUMBER_OF_PROCESSORS']}")
    public void setProcessors(int processors) {
        this.processors = processors;
    }

    public String getOwnerName() {
        return ownerName;
    }

    @Value("#{systemEnvironment['USERNAME']}")
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOs() {
        return os;
    }

    @Value("#{systemProperties['os.name']}")
    public void setOs(String os) {
        this.os = os;
    }

    public List<String> getFeatures() {
        return features;
    }

    @Value("#{{'24gb RAM', '500GB', 'os: ' + systemProperties['os.name'], " +
            "'Processors: ' + systemEnvironment['NUMBER_OF_PROCESSORS'], " +
            "'CPU: ' + systemProperties['sun.cpu.isalist']}}")
    public void setFeatures(List<String> features) {
        this.features = features;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "purchaseDate=" + purchaseDate +
                ", vendorUrl=" + vendorUrl +
                ", price=" + price +
                ", memory=" + memory +
                ", cpu='" + cpu + '\'' +
                ", processors=" + processors +
                ", ownerName='" + ownerName + '\'' +
                ", os='" + os + '\'' +
                ", features=" + features +
                '}';
    }
}