package be.janolaerts.corespringopdracht15.aspects;

public interface MusicMaker {

    void makeMusic(String song);
}