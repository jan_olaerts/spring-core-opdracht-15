package be.janolaerts.corespringopdracht15.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Aspect
@PropertySource("classpath:application.properties")
public class TimeAspect {

    @Value("${minHourForVacuumCleaner}")
    private int minHourForVacuumCleaner;

    @Value("${maxHourForVacuumCleaner}")
    private int maxHourForVacuumCleaner;

    @Before("execution(public void be.janolaerts.corespringopdracht15.housekeeping.services.cleaning.VacuumCleaner.doCleanJob())")
    public void checkTimeForVacuumCleaner() {
        int now = LocalDateTime.now().getHour();

        if(now > minHourForVacuumCleaner && now < maxHourForVacuumCleaner)
            throw new RuntimeException("Cannot use vacuum cleaner between " + minHourForVacuumCleaner + " and " + maxHourForVacuumCleaner);
    }
}