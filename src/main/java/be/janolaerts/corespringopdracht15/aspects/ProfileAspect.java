package be.janolaerts.corespringopdracht15.aspects;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Aspect
public class ProfileAspect {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());

    @Pointcut("execution(public void runHousehold())")
    private void forRunHouseHold() {}

    @Around("forRunHouseHold()")
    public void measureTimeToExecuteRunHousehold(ProceedingJoinPoint pjp) throws Throwable {
        long beforeRunHousehold = System.currentTimeMillis();
        pjp.proceed();
        long afterRunHousehold = System.currentTimeMillis();
        long totalTime = afterRunHousehold - beforeRunHousehold;
        logger.info("It took " + totalTime / 1000.0 + " seconds to execute runHousehold()");
    }
}