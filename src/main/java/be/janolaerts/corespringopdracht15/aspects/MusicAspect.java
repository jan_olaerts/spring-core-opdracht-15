package be.janolaerts.corespringopdracht15.aspects;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MusicAspect {

    @Pointcut("execution(public void runHousehold())")
    private void forRunHousehold() {}

    @DeclareParents(value="be.janolaerts.corespringopdracht15.housekeeping.services.domestic.DomesticServiceImpl",
                    defaultImpl=be.janolaerts.corespringopdracht15.aspects.MusicImpl.class)
    public static MusicMaker mixin = new MusicImpl();

    @Before("forRunHousehold()")
    public void beforeRunHousehold() {
        singSong("Fly Me To The Moon");
    }

    @After("forRunHousehold()")
    public void afterRunHousehold() {
        singSong("Fly Me To The Moon");
    }

    public void singSong(String song) {
        mixin.makeMusic(song);
    }
}