package be.janolaerts.corespringopdracht15.aspects;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Aspect
public class ReportAspect {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());

    @Pointcut("execution(public void runHousehold())")
    private void forRunHouseHold() {}

    @Before("forRunHouseHold()")
    public void beforeRunHouseholdAdvice(JoinPoint jp) {
        logger.info("Before " + jp.getSignature().getName() + " method");
    }

    @After("forRunHouseHold()")
    public void afterRunHouseholdAdvice(JoinPoint jp) {
        logger.info("After " + jp.getSignature().getName() + " method");
    }

    @AfterThrowing(value = "forRunHouseHold()", throwing="exception")
    public void afterRunHouseholdExceptionAdvice(JoinPoint jp, Throwable exception) {
        logger.info("Exception in " + jp.getSignature().getName());
        logger.info("Exception: " + exception.getMessage());
    }
}