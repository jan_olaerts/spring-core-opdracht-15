package be.janolaerts.corespringopdracht15.aspects;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class MusicImpl implements MusicMaker {

    private final Logger logger = Logger.getLogger(LoggerConfig.class.getName());

    @Override
    public void makeMusic(String song) {
        logger.info("Singing " + song);
    }
}