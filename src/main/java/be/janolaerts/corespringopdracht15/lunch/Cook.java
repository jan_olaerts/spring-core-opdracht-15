package be.janolaerts.corespringopdracht15.lunch;

import be.janolaerts.corespringopdracht15.logging.LoggerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class Cook {

    private Logger logger = Logger.getLogger(LoggerConfig.class.getName());

    @Autowired
    private ApplicationEventPublisher publisher;

    @EventListener(classes=ContextStartedEvent.class)
    @Order(2)
    public void makeLunch() {
        logger.info("Cook making lunch");
        publisher.publishEvent(new LunchEvent());
    }
}