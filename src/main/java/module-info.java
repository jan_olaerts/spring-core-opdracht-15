open module be.janolaerts.corespringopdracht15 {
    requires spring.core;
    requires spring.context;
    requires spring.beans;
    requires java.sql;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.boot.starter.aop;
    requires java.logging;
    requires java.annotation;
    requires org.aspectj.weaver;
}